--[[ Exports ]]--
local element1Name = "Name" --export: Name of slot "element1"
local element2Name = "Name" --export: Name of slot "element2"
local element3Name = "Name" --export: Name of slot "element3"
local element4Name = "Name" --export: Name of slot "element4"
local element5Name = "Name" --export: Name of slot "element5"
local element6Name = "Name" --export: Name of slot "element6"
local element7Name = "Name" --export: Name of slot "element7"
local element8Name = "Name" --export: Name of slot "element8"
local element9Name = "Name" --export: Name of slot "element9"

local elements = {
    {name=element1Name, element=element1},
    {name=element2Name, element=element2},
    {name=element3Name, element=element3},
    {name=element4Name, element=element4},
    {name=element5Name, element=element5},
    {name=element6Name, element=element6},
    {name=element7Name, element=element7},
    {name=element8Name, element=element8},
    {name=element9Name, element=element9},
}

function updateScreenText()
    local rowHTML = ""

    for _, v in ipairs(elements) do
        if v.element == nil then
            goto continue
        end

        local state = v.element.getState()
        local info = v.element.getInfo()

        local stateDescription = "Running"
        local stateColor = "cyan"
        if state == 1 then
            stateDescription = "Stopped"
            stateColor = "white"
        elseif info["stopRequested"] then
            stateDescription = "Stopping"
            stateColor = "orange"
        elseif state == 3 then
            stateDescription = "Ingredient missing"
            stateColor = "yellow"
        elseif state == 4 then
            stateDescription = "Output full"
            stateColor = "yellow"
        elseif state == 5 then
            stateDescription = "No Output Container"
            stateColor = "red"
        elseif state == 6 then
            stateDescription = "Pending"
            stateColor = "blue"
        elseif state == 7 then
            stateDescription = "No schematics"
            stateColor = "red"
        end

        local productionType = "Batch"
        local productionAmount = ""
        if info["maintainProductAmount"] > 0 then
            productType = "Maintain"
            productionAmount = string.format("%d / %d", info["currentProductAmount"] / 16777216, info["maintainProductAmount"])
        end


        rowHTML = rowHTML..[[
        <tr>
            <td>]]..v.name..[[</td>
            <td style="color:]]..stateColor..[[">]]..stateDescription..[[</td>
            <td>]]..productionType..[[<br>]]..productionAmount..[[</td>
        </tr>
        ]]

        ::continue::
    end

    screen.setHTML([[
    <header>
        <style>
        * {
            font-size: 20px;
        }
        td {
            border-bottom: 1px solid grey;
            border-top: 1px solid grey;
            border-collapse: collapse;
        }
        </style>
    </header>
    <body>
        <div class="bootstrap">
            <table style="width:100%;height:100%">
                <tr>
                    <td>Name</td>
                    <td>State</td>
                    <td>Production</td>
                </tr>
                ]]..rowHTML..[[
            </table>
        </div>
    </body>
    ]])
end

screen.activate()
updateScreenText()

unit.setTimer("updateScreen", 10)
