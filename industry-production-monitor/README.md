# Industry Production Monitor

![screens](img/screens.jpg)

## Requirements

- 1x Programming Board
- 1x Screen (any size, preferred M or greater)

## Installation

- Name one slot `screen` and the others `element1` to `element9`.
- Create a filter `onStart()` in `unit` and copy the code from the lua file [unit.onStart.lua](unit.onStart.lua) into the editor.
- Create a filter `onStop()` in `unit` and copy the code from the lua file [unit.onStop.lua](unit.onStop.lua) into the editor.
- Create a filter `onTimer(updateScreen)` in `unit` and copy the code from the lua file [unit.onTimer-updateScreen.lua](unit.onTimer-updateScreen.lua) into the editor.
- Edit Lua parameters and set up the names for each `element` slot shown on the screen.
