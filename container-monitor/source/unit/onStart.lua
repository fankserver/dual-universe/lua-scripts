containerPrefix = "SC1_" --export: the prefix used to check containers
containerHubPrefix = "SH1_" --export: the prefix used to check container hubs
containerProficiencyLevel = 5 --export: Container Proficiency talent level

oreList = {
    -- T1
    Hematite = {mass = 5.04},
    Iron = {mass = 7.85},
    Bauxite = {mass = 1.2811},
    Aluminium = {mass = 2.7},
    Quartz = {mass = 2.65},
    Silicon = {mass = 2.33},
    Coal = {mass = 1.35},
    Carbon = {mass = 2.27},
    -- T2
    Limestone = {mass = 2.711},
    Calcium = {mass = 1.55},
    Malachite = {mass = 4},
    Copper = {mass = 8.96},
    Natron = {mass = 1.55},
    Sodium = {mass = 0.97},
    Chromite = {mass = 4.54},
    Chromium = {mass = 7.19},
    -- T3
    Pyrite = {mass = 5.01},
    Sulfur = {mass = 1.82},
    Petalite = {mass = 2.41},
    Lithium = {mass = 0.53},
    Acanthite = {mass = 7.2},
    Silver = {mass = 10.49},
    Garnierite = {mass = 2.6},
    Nickel = {mass = 8.91},
    -- T4
    GoldNuggets = {mass = 19.3},
    Gold = {mass = 19.3},
    Cryolite = {mass = 2.95},
    Fluorine = {mass = 1.7},
    Cobaltite = {mass = 6.33},
    Cobalt = {mass = 8.9},
    Kolbeckite = {mass = 2.37},
    Scandium = {mass = 2.98},
    -- T5
    Rhodonite = {mass = 3.76},
    Manganese = {mass = 7.21},
    Columbite = {mass = 5.38},
    Niobium = {mass = 8.57},
    Vanadinite = {mass = 6.95},
    Vanadium = {mass = 6},
    Ilmenite = {mass = 4.55},
    Titanium = {mass = 4.51},
    -- OTHER
    Oxygen = {mass = 1},
    Hydrogen = {mass = 0.07},
    Warpcell = {mass = 100},
}

renderScript = [[
local json = require('dkjson')
local input = getInput() or json.encode(nil)
local data = json.decode(input)

if data ~= nil then
    items = data
end

if items == nil then
    items = {}
end

---------------------------------------

local rx, ry = getResolution()
local vw = rx/100
local vh = ry/100
local layer = createLayer()

Font = loadFont(getAvailableFontName(5), 5*vh)
FontSmall = loadFont(getAvailableFontName(5), 3*vh)

gaugeR = 0.7
gaugeG = 0.4
gaugeB = 0
gaugeSteps = 10
guageBarType = 1
guageFlair = 1
sortLeftToRight = true

-- grid
setDefaultStrokeColor(layer, Shape_Line, 0.7, 0.4, 0, 1)
addLine(layer, 0, 0, rx, 0)
addLine(layer, 0, ry/4, rx, ry/4)
addLine(layer, 0, (ry/4)*2, rx, (ry/4)*2)
addLine(layer, 0, (ry/4)*3, rx, (ry/4)*3)
addLine(layer, 0, ry, rx, ry)
addLine(layer, rx/2, 0, rx/2, ry)

function VerticalGauge(Data,X,Y,SX,SY,n,r,g,b,BarType,Flair)

    Height = math.ceil(Data/(100/n))

    if BarType == 1 then

        for jj = 1,Height,1 do
            setNextFillColor(layer,r,g,b,0.2+(jj^3)*(0.8/(Height^3)))
            addQuad(layer,
                X - SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.2,
                X - SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.8,
                X + SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.8,
                X + SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.2)
        end

    elseif BarType == 2 then

        setNextFillColor(layer,r,g,b,1)
        addQuad(layer,
            X - SX/2,
            Y+SY/2,
            X - SX/2,
            Y+SY/2 - SY*Data/100,
            X + SX/2,
            Y+SY/2 - SY*Data/100,
            X + SX/2,
            Y+SY/2)

    elseif BarType == 3 then

        setNextFillColor(layer,r,g,b,1)
        addQuad(layer,
            X - SX/2 - vh,
            Y+SY/2 - SY*Data/100 + vh,
            X - SX/2 - vh,
            Y+SY/2 - SY*Data/100 - vh,
            X + SX/2 + vh,
            Y+SY/2 - SY*Data/100 - vh,
            X + SX/2 + vh,
            Y+SY/2 - SY*Data/100 + vh)

    elseif BarType == 4 then

        setNextFillColor(layer,r,g,b,1)
        addQuad(layer,
            X - SX/2 - vh,
            Y+SY/2 - SY*Data/100 + vh,
            X - SX/2 - vh,
            Y+SY/2 - SY*Data/100 - vh,
            X + SX/2 + vh,
            Y+SY/2 - SY*Data/100 + vh,
            X + SX/2 + vh,
            Y+SY/2 - SY*Data/100 - vh)

    elseif BarType == 5 then

        for jj = 1,Height,1 do
            setNextFillColor(layer,r,g,b,0.2+(jj^3)*(0.8/(Height^3)))
            addQuad(layer,
                X - SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.1,
                X - SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.6,
                X + SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*1.1,
                X + SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.6)
        end

    elseif BarType == 6 then

        for jj = 1,Height,1 do
            setNextFillColor(layer,r,g,b,0.2+(jj^3)*(0.8/(Height^3)))
            addQuad(layer,
                X - SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.1,
                X - SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.6,
                X - 1,
                Y+SY/2 - (jj-1)*SY/n - SY/n*1.1,
                X - 1,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.6)
            setNextFillColor(layer,r,g,b,0.2+(jj^3)*(0.8/(Height^3)))
            addQuad(layer,
                X,
                Y+SY/2 - (jj-1)*SY/n - SY/n*1.1,
                X,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.6,
                X + SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.1,
                X + SX/2,
                Y+SY/2 - (jj-1)*SY/n - SY/n*0.6)

        end

    end




    -- Flair --

    if Flair == 1 then

        setNextStrokeColor(layer,r,g,b,1)
        setNextStrokeWidth(layer,0.5*vh)
        setNextFillColor(layer,0,0,0,0)
        addBoxRounded(layer, X - SX/2 - vh, Y - SY/2 - vh, SX + 2*vh, SY + 2*vh, 1*vh)

    elseif Flair == 2 then

        setNextStrokeColor(layer,r,g,b,1)
        setNextStrokeWidth(layer,0.5*vh)
        addLine(layer, X - SX/2 - vh, Y - SY/2 - vh, X - SX/2 - vh, Y - SY/2 - vh + SY*0.3 )

        setNextStrokeColor(layer,r,g,b,1)
        setNextStrokeWidth(layer,0.5*vh)
        addLine(layer, X - SX/2 - vh, Y - SY/2 - vh, X - SX/2 - vh + SX*0.4, Y - SY/2 - vh )

        setNextFillColor(layer,r,g,b,1)
        addCircle(layer,X - SX/2 - vh, Y - SY/2 - vh, 1*vh)

        setNextStrokeColor(layer,r,g,b,0.5)
        setNextStrokeWidth(layer,0.25*vh)
        addLine(layer, X + SX/2 + vh, Y + SY/2 + vh, X + SX/2 + vh, Y + SY/2 + vh - SY*0.2 )

        setNextStrokeColor(layer,r,g,b,0.5)
        setNextStrokeWidth(layer,0.25*vh)
        addLine(layer, X + SX/2 + vh, Y + SY/2 + vh, X + SX/2 + vh - SX*0.4, Y + SY/2 + vh )

        setNextFillColor(layer,r,g,b,1)
        addCircle(layer,X + SX/2 + vh, Y + SY/2 + vh, 0.5*vh)

    elseif Flair == 3 then

        setNextFillColor(layer,r,g,b,0.1)
        addBox(layer, X - SX/2, Y - SY/2, SX, SY, 1*vh)

    end
end

function DrawContainerInfo(Row,Column,Name,CurrentL,MaximumL)

    setNextFillColor(layer, 1, 1, 1, 1)
    local nameX = 10*vw
    local gaugeX = 22*vw
    local contentX = 37.2*vw
    local contentY = Row*25*vh-25*vh/2
    if Column == 2 then
        nameX = rx - nameX
        gaugeX = rx - gaugeX
        contentX = rx - contentX
    end

    if Row == 1 or Row == 2 then
        setNextTextAlign(layer, AlignH_Center, AlignV_Bottom)
        addText(layer, Font, Name, nameX, (25*Row)*vh-2*vh)
    elseif Row == 3 or Row == 4 then
        setNextTextAlign(layer, AlignH_Center, AlignV_Top)
        addText(layer, Font, Name, nameX, (50+(Row-3)*25)*vh+2*vh)
    end

    VerticalGauge(CurrentL/MaximumL*100, gaugeX, (ry/8)*(Row*2-1), 3*vw, 20*vh, gaugeSteps, gaugeR, gaugeG, gaugeB, guageBarType, guageFlair)

    setNextTextAlign(layer, AlignH_Left, AlignV_Middle)
    addText(layer, FontSmall, "Current", contentX-12*vw, contentY-3*vh)
    setNextTextAlign(layer, AlignH_Left, AlignV_Middle)
    addText(layer, FontSmall, "Maximum", contentX-12*vw, contentY+3*vh)
    setNextTextAlign(layer, AlignH_Right, AlignV_Middle)
    addText(layer, Font, string.format("%.0fL", CurrentL), contentX+12*vw, contentY-3*vh)
    setNextTextAlign(layer, AlignH_Right, AlignV_Middle)
    addText(layer, Font, string.format("%dL", MaximumL), contentX+12*vw, contentY+3*vh)
end

----------------------------------------------------

local Row = 1
local Column = 1
if sortLeftToRight then
    Column = 2
end

local screenSlotAmount = 8
local screenSlotIndex = 1

for i = screenSlotIndex, screenSlotAmount, 1 do
    for _,v in pairs(items) do
        if v.position == i then
            DrawContainerInfo(Row, Column, v.name, v.currentVolume, v.maxVolume)
            break
        end
    end

    Row = Row + 1
    if Row > 4 then

        if sortLeftToRight then
            Column = Column - 1
        else
            Column = Column + 1
        end
        Row = 1
    end

    if not sortLeftToRight and Column > 2 then
        break
    elseif sortLeftToRight and Column == 0 then
        break
    end
end

requestAnimationFrame(60)
]]



elementsIdList = {}
if core ~= nil then
    elementsIdList = core.getElementIdList()
end
storageIdList = {}

screens = {}
for slot_name, slot in pairs(unit) do
    if
    type(slot) == "table"
        and type(slot.export) == "table"
        and slot.getClass
        and slot.getClass():lower() == 'screenunit'
    then
        slot.slotname = slot_name
        table.insert(screens,slot)
    end
end

table.sort(screens, function(a,b) return a.slotname < b.slotname end)

for _,s in pairs(screens) do
    s.activate()
    s.setRenderScript(renderScript)
    s.setScriptInput("{}")
end

local function split(str, sep)
    local result = {}
    local regex = ("([^%s]+)"):format(sep)
    for each in str:gmatch(regex) do
        table.insert(result, each)
    end
    return result
end

for i = 1, #elementsIdList, 1 do
    local id = elementsIdList[i]
    local elementType = core.getElementDisplayNameById(id):lower()
    if elementType:lower():find("container") then
        local elementName = core.getElementNameById(id)
        if
            elementName:lower():find(containerPrefix:lower())
            or elementName:lower():find(containerHubPrefix:lower())
        then
            table.insert(storageIdList, id)
        end
    end
end

function updateScreens()
    local storages = {}

    for i = 1, #storageIdList, 1 do
        local id = storageIdList[i]
        local elementName = core.getElementNameById(id)
        local storage = {}

        local containerVolumeList = {xxl=512000, xl=256000, l=128000, m=64000, s=8000, xs=1000}
        local containerAmount = 1
        local containerMass = 0
        local containerSize = "XS"

        local nameParts = split(elementName, "_")
        if elementType:lower():find("hub") then
            storage.name = nameParts[4]
            storage.position = tonumber(nameParts[2])

            containerAmount = tonumber(string.match(nameParts[3], "%d+"))
            containerSize = string.sub(nameParts[3], -1)
            containerMass = core.getElementMassById(id) - 55.8 -- 55.8 is the hub mass

            local volume = 0
            containerSize = containerSize:lower()
            if containerVolumeList[containerSize] then
                volume = containerVolumeList[containerSize]
            end


            local unitMass = 0
            if oreList[storage.name] then
                unitMass = oreList[storage.name].mass
            end

            storage.maxVolume = (volume * containerProficiencyLevel * 0.1 + volume) * containerAmount
            if unitMass > 0 then
                storage.currentVolume = containerMass / unitMass
            else
                storage.currentVolume = 0
            end
        else
            storage.name = nameParts[3]
        end

        table.insert(storages, storage)
    end

    local screenSlotAmount = 8
    local screenIndex = 1
    for _,screen in pairs(screens) do
        local screenSlotStart = screenSlotAmount * (screenIndex - 1) + 1
        local screenSlotEnd = screenSlotAmount * screenIndex

        local screenStorages = {}
        for i = screenSlotStart, screenSlotEnd, 1 do
            for _,storage in pairs(storages) do
                if storage.position == i then
                    storage.position = storage.position - screenSlotStart + 1
                    table.insert(screenStorages, storage)
                    break
                end
            end
        end

        screen.setScriptInput(json.encode(screenStorages))
        screenIndex = screenIndex + 1
    end
end

updateScreens()
unit.setTimer("refresh", 10)
